#!/bin/sh
BRPATH=/malina/pr_20160422/buildroot-2016.02
APP_NAME=board_test
APP_DIR=/tmp/app
RPI_ADDRESS=192.168.143.235

export PATH=$BRPATH/output/host/usr/bin:$PATH
make ARCH=arm CROSS_COMPILE=arm-none-linux-gnueabi- $APP_NAME && \
scp $APP_NAME root@$RPI_ADDRESS:$APP_DIR

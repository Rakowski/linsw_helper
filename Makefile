Makefile:
CC=$(CROSS_COMPILE)gcc	

OBJS := main.o
board_test: $(OBJS)
	$(CC) -o board_test $(OBJS)

$(OBJS) : %.o : %.c
	$(CC) -c $(CFLAGS) $< -o $@
